# Workshop on Privacy, Trust, and Data Equity

CommunityBridge.com is hosting a series of Privacy Workshops using movies, classes, and hands-on sessions on regaining freedom in the digital world and taking back control of your data.

Visit <https://communitybridge.com/privacyworkshop/> for information about our community and <https://communitybridge.com/events/> for the calendar of public events.

_**This repository contains materials from the community meetings, shared documents, and project management artifacts.**_
